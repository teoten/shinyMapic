FROM rocker/shiny:latest

RUN apt-get update && apt-get install -y libcurl4-openssl-dev libnode-dev git
RUN mkdir -p /usr/local/lib/R/etc/ /usr/lib/R/etc/
RUN mkdir -p /home/share/

# Download and install libraries for mapic
RUN R -e "install.packages('remotes')"
RUN R -e "install.packages(c('rhandsontable', 'shiny', 'shinyalert', 'shinyjs', 'shinyWidgets', 'dbplyr'))"
RUN R -e "install.packages(c('cowplot', 'DBI', 'DT', 'ggplot2', 'pkgload', 'config', 'purrr'))"
RUN R -e "install.packages(c('markdown', 'grid', 'htmltools', 'png', 'RSQLite', 'shinyFeedback', 'stringr'))"
RUN Rscript -e 'remotes::install_git("https://codeberg.org/teoten/mapic", dependencies = TRUE)'

# Install the app as package
RUN mkdir /build_zone
ADD . /build_zone
WORKDIR /build_zone
RUN R -e 'remotes::install_local(upgrade="never")'
#RUN rm -rf /build_zone

EXPOSE 3838

CMD R -e "options('shiny.port'=3838,shiny.host='0.0.0.0');library(shinyMapic);shinyMapic::run_app()"
