* Development
** DONE Add option to download image
** DONE Include logo and Copyright
** DONE Update docker file (include missing packages)
** DONE Include prod colors in pre-prod
** TODO [#B] Add instructions section
** TODO [#C] Add script/function to clean up temporal DB
** DONE Use of YAML file
* Improvements
** DONE Go through all TODO's across the project scripts
** TODO [#B] CSS
* Bugs
** DONE =add_copyrigh_to_map= add to mapic object
** DONE It is not removing "closed" entries
