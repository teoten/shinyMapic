# About
This section contains instructions and FAQ for the usage of the app.

# Usage
The app is divided into 3 main sections that can be explored using the tabs at the top bar. 
Each of them has a specific task in the functioning of the app.

The sections are:
 - **Intro:** This is the main view of the data and works as an introduction to the app.
 - **Coords:** In charge of retrieval of the coordinates needed to make the maps.
 - **Maps:** The section that actually creates the maps.

## Intro
This section can be used only when there is already existent data. 
There are 2 sources of data that can be called here:

### Session
Data from a working session can be called using the "Session No.", which can be found at the top-right corner of the app.
 - **Data Base:**

## Coords

## Maps

# Suggested workflow
